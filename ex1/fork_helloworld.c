#include <stdio.h>
#include <stdlib.h>

int main (int argc, char **argv){
  printf("hello world is staring\n");
  int pid;
  pid = fork();
  if(pid == 0){
    for (int a = 0; a < 5; a++){

      printf("I am a child process with id = %d\n", pid);
      char *line;
      scanf("input char%s", line);
    }
  } else {
    for (int a = 0; a < 5; a++){
      printf("I am the parent process with id = %d\n", pid);
  }
}
}
