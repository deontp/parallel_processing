import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.rmi.RemoteException;
import java.rmi.Naming;
import java.io.Console;
import java.util.*;

public class MSGClient {

  MessageInterface msg;
  Hashtable<String, ArrayList<String>> mailbox;

  public void init(){
    try{
      Registry reg = LocateRegistry.getRegistry();
      System.out.println("Connecting...");
      msg = (MessageInterface)     Naming.lookup("rmi://localhost:2000/MessengerService");
      System.out.println("Connected...");
    } catch (Exception e){
      e.printStackTrace();
    }
  }

  public void sendMessage(String username, String message){
    try {
        msg.storeMessage(username, message);
    } catch (RemoteException e){
        e.printStackTrace();
    }
  }

  public void getMessages(String username){
    try {
      mailbox = msg.getMessages(username);
    } catch (RemoteException e){
      e.printStackTrace();
    }
    if (mailbox.size() == 0){
      System.out.println("no messages");
    } else{
      System.out.println(mailbox);
    }
  }

  public static void main (String[] argv) {
    MSGClient m = new MSGClient();
    m.init();
    Console cnsl = null;
    String msg = "";
    String un = "";
    String res = "";
    Boolean running = true;
    while (running){
      try {
        cnsl = System.console();
        if (cnsl != null){
          res = cnsl.readLine("Make Selection:\n1) Send message \n2) View mailbox\n3) Exit\nInput:");
          switch (res){
            case "1":
              un = cnsl.readLine("Enter Username:");
              msg = cnsl.readLine("Enter Message:");
              m.sendMessage(un, msg);
              break;
            case "2":
              m.getMessages("Deon");
              break;
            case "3":
              running = false;
              break;
            default:
              System.out.println("Not a valid response please try again");
              break;
          }

        }
      } catch (Exception e){
        e.printStackTrace();
      }
    }

  } //main
}// MSGClient
