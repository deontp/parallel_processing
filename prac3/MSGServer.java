import java.rmi.server.UnicastRemoteObject;
import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.rmi.RemoteException;
import java.util.*;

public class MSGServer implements MessageInterface{

  private static Hashtable<String, ArrayList<String>> messageStore;

  public void storeMessage(String username, String msg){
    ArrayList<String> a = messageStore.get(username);
    if (a == null){
      a = new ArrayList();
      a.add(msg);

    } else{
       a.add(msg);
    }
    messageStore.put(username, a);
  }

  public Hashtable getMessages (String username){
    return messageStore;

  }

  public static void main (String[] argv){
    try {
      System.out.println("Creating...");
      MSGServer s = new MSGServer();
      MessageInterface stub = (MessageInterface) UnicastRemoteObject.exportObject(s, 0);
      Registry reg = LocateRegistry.createRegistry(2000);
      reg.bind("MessengerService", stub);
      messageStore = new Hashtable<String, ArrayList<String>>();
      System.out.println("Running...");
  } catch (Exception e){
    e.printStackTrace();
  }
  }// main
}// MSGServer
