import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.*;

public interface MessageInterface extends Remote{
  public void storeMessage(String username, String message) throws RemoteException ;
  public Hashtable<String, ArrayList<String>> getMessages(String message) throws RemoteException;
}// MessageInterface
