import org.jcsp.lang.*;

/** Main program class for Producer/Consumer example.
  * Sets up channel, creates one of each process then
  * executes them in parallel, using JCSP.
  */
public final class PCMain{

  public static void main (String[] args){
    new PCMain();
  } // main

  public PCMain (){
    // Create channel object
    //final One2OneChannelInt chP1toB = Channel.one2oneInt();
    //final One2OneChannelInt chC1toB = Channel.one2oneInt();
    //final One2OneChannelInt chC1toBReq = Channel.one2oneInt();
    // Subscripts denote the producer or consumer eg Prod1 = array[0]
    final One2OneChannelInt[] producers = {Channel.one2oneInt(), Channel.one2oneInt()};
    final One2OneChannelInt[] consumers = {Channel.one2oneInt(), Channel.one2oneInt()};
    final One2OneChannelInt[] requests = {Channel.one2oneInt(), Channel.one2oneInt()};

    // Create and run parallel construct with a list of processes
    /*
    CSProcess[] procList = {
      new Producer(chP1toB.out(), 0),
      new Buffer(chP1toB.in(), chC1toB.out(), chC1toBReq.in()),
      new Consumer(chC1toB.in(), chC1toBReq.out())
    };
    */

    CSProcess[] procList = {
      new Producer(producers[0].out(), 0), // producer 1
      new Producer(producers[1].out(), 100), // producer 2
      new Buffer(producers, consumers, requests),
      new Consumer(consumers[0].in(), requests[0].out()), // consumer 1
      new Consumer(consumers[1].in(), requests[1].out()) // consumer 2
    }; // Processes
    Parallel par = new Parallel(procList); // PAR construct
    par.run(); // Execute processes in parallel
  } // PCMain constructor
} // class PCMain
