import org.jcsp.lang.*;

/** Producer class: produces one random integer and sends on
  * output channel, then terminates.
  */
public class Producer implements CSProcess{
  private ChannelOutputInt out;
  private int value;

    public Producer (final ChannelOutputInt out, Integer value){
      this.out = out;
      this.value = value;
    } // constructor

    public void run (){
      for (int a = 0; a < 100; a++){
        int item = (int)(Math.random()*100)+1 + value;
        out.write(item);
      }
      out.write(-1); // done sending numbers
    } // run

  } // class Producer
