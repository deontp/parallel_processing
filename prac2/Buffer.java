import org.jcsp.lang.*;

public class Buffer implements CSProcess{
  private AltingChannelInputInt inA;
  private ChannelOutputInt outA;
  private AltingChannelInputInt reqA;

  private AltingChannelInputInt inB;
  private ChannelOutputInt outB;
  private AltingChannelInputInt reqB;

  private Integer[] store = new Integer[10];
  private int active;; // the number of processes that are running



  //public Buffer (final AltingChannelInputInt in, final ChannelOutputInt out, final AltingChannelInputInt req){
  public Buffer (final One2OneChannelInt[] Producers, final One2OneChannelInt[] Consumers, final One2OneChannelInt[] Requests){
    this.inA = Producers[0].in();
    this.outA = Consumers[0].out();
    this.reqA = Requests[0].in();

    this.inB = Producers[1].in();
    this.outB = Consumers[1].out();
    this.reqB = Requests[1].in();

    active = Producers.length + Consumers.length; //the number of producers and consumers in total
  }


  public void run(){
    int head = 0;
    int tail = 0;
    int items = 0;
    int temp = 0;

    final Alternative alt = new Alternative(new Guard[]{inA, reqA, inB, reqB});
    boolean [] bools = new boolean[4];
    while (active > 0){
      bools[0] = items < 10 ; // space to add more to the array
      bools[1] = items > 0; // items to read

      bools[2] = items < 10 ; // space to add more to the array
      bools[3] = items > 0; // items to read
      switch  (alt.select(bools)){
        case 0: // in from p1
          int inputA = inA.read();
          if (inputA == -1){
            active--;
          }
          store[head % 10] = inputA;
          items++;
          head++;
          break;
        case 1: // out to p2
          int outputA = store[tail % 10];
          if (outputA == -1){
            active--;
          }
          reqA.read();
          outA.write(outputA);
          temp++;
          items--;
          tail++;
          break;
        case 2: // in from p1
          int inputB = inB.read();
          if (inputB == -1){
            active--;
          }
          store[head % 10] = inputB;
          items++;
          head++;
          break;
        case 3: //out to p2
          int outputB = store[tail % 10];
          if (outputB == -1){
            active--;
          }
          reqB.read();
          outB.write(outputB);
          temp++;
          items--;
          tail++;
          break;
      }
    }
  }

}
