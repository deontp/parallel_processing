import org.jcsp.lang.*;

/** Consumer class: reads one int from input channel, displays it, then
  * terminates.
  */
public class Consumer implements CSProcess{
  private ChannelInputInt in;
  private ChannelOutputInt req;

  public Consumer (final ChannelInputInt in, final ChannelOutputInt req){
    this.in = in;
    this.req = req;
  } // constructor

  public void run (){
    while (true){
      req.write(0);
      int item = in.read();
      if (item == -1){
        break;
      } else{
        System.out.println(item);
      }
    }
    System.out.println("Closed");

  } // run

} // class Consumer
