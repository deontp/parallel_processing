Q1 Mandelbrot:
- Changes made on the following lines:
  * 81 - 94
  * 238
  * 292
- I chose to use the newFixedThreadPool executor service because of the fact that my lab computer only has 4 cores thus allowing on 4 threads to be executed at a time. I supplied the newFixedThreadPool with 5 threads just to have the extra thread available. 
Q2 Quick Sort
- For a list size of 1 000 000 we get the following average over 10 runs:
  * SEQ = 3740.4 ms
  * PAR = 1156.3 ms
  * speedup = 3740.4 / 1156.3 = 3.23
-
