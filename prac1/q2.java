import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ForkJoinPool;

class q2 {

  final static int MAX = 1000000;
  final static int ITR = 10;

  private static void printResults(Double time, String alg){
    /*
    for (int i = 0; i < value.length; i++){
      System.out.print(value[i]);
      System.out.print(' ');
    }
    */
    System.out.format("\n%s Sorting size %d took %f ms\n", alg, MAX, time);
}
  private static Integer[] createArray(){
    Random rnd = new Random();
    Integer[] a = new Integer[MAX];
    for (int i = 0; i < a.length; i++){
      int num = rnd.nextInt(100);
      a[i] = num;
    }
    return a;
  }

  public static void main (String args[]){


    long startTime, endTime, duration;
    ForkJoinPool pool = new ForkJoinPool();
    Double avgA, avgB = 0.0;
    long total = 0;

    for (int i = 0; i < ITR; i++){
      Integer[] a = createArray();
      startTime = System.currentTimeMillis();
      SortSEQ.quickSort(a);
      endTime = System.currentTimeMillis();
      total += (endTime - startTime);
    }
    avgA = (total / 10.0);
    total = 0;

    for(int i = 0; i < ITR; i++){
      Integer[] b = createArray();
      SortPAR sp = new SortPAR(b, 0, b.length-1);
      startTime = System.currentTimeMillis();
      pool.invoke(sp);
      endTime = System.currentTimeMillis();
      total += (endTime - startTime);
    }
    avgB = (total / 10.0);

    printResults(avgA, "SEQ");
    printResults(avgB, "PAR");
  }

} // q2
